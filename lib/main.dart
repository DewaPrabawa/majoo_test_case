import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo_test_case/app_bloc_observer.dart';
import 'package:majoo_test_case/preload.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'di_container/di.dart';
import 'features/feed/presentation/cubit/movie_cubit.dart';
import 'features/feed/presentation/view/home_view.dart';
import 'features/login_register/presentation/auth_cubit/auth_cubit.dart';
import 'features/login_register/presentation/login_cubit/login_cubit.dart';
import 'features/login_register/presentation/register_cubit/register_cubit.dart';
import 'features/login_register/presentation/view/register_view.dart';

void main() {
  BlocOverrides.runZoned(() async {
    WidgetsFlutterBinding.ensureInitialized();
    await init();
    runApp(const MyApp());
  }, blocObserver: AppBlocObserver());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthCubit>(
          create: (context) => sl<AuthCubit>()..isLoggin(),
        ),
        BlocProvider<LoginCubit>(create: (context) => sl<LoginCubit>()),
        BlocProvider<RegisterCubit>(create: (context) => sl<RegisterCubit>())
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Demo',
        onGenerateRoute: Routing.registeredRoute,
        home: Preload(),
      ),
    );
  }
}

class Routing {
  static Route<dynamic> registeredRoute(RouteSettings settings) {
    switch (settings.name) {
      case "/preload":
        return _buildPreloadRoute(settings);
      case "/login":
        return _buildLoginRoute(settings);
      case "/register":
        return _buildRegisterRoute(settings);
      default:
        return _buildPreloadRoute(settings);
    }
  }

  static _buildLoginRoute(RouteSettings settings) {
    final homePage = BlocProvider<MovieTrendingCubit>(
      create: (context) => sl<MovieTrendingCubit>()..getFeeds(),
      child: HomeView(),
    );
    return MaterialPageRoute(
        settings: settings, builder: (context) => homePage);
  }

  static _buildRegisterRoute(RouteSettings settings) {
    return MaterialPageRoute(
        settings: settings,
        builder: (context) => BlocProvider<RegisterCubit>(
              create: ((context) => RegisterCubit(sl())),
              child: const RegisterView(),
            ));
  }

  static _buildPreloadRoute(RouteSettings settings) {
    return MaterialPageRoute(
        settings: settings, builder: (context) => const Preload());
  }
}
