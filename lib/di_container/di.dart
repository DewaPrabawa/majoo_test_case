import 'dart:developer';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:get_it/get_it.dart';
import 'package:majoo_test_case/features/feed/data/remote_service/feed_remote_service.dart';
import 'package:majoo_test_case/features/feed/data/repository/feed_repository.dart';
import 'package:majoo_test_case/features/feed/domain/feed_usecase.dart';
import 'package:majoo_test_case/features/feed/presentation/cubit/movie_cubit.dart';
import 'package:majoo_test_case/features/login_register/domain/auth_use_case.dart';
import 'package:majoo_test_case/features/login_register/presentation/auth_cubit/auth_cubit.dart';

import 'package:majoo_test_case/shared/network_status.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../features/login_register/presentation/login_cubit/login_cubit.dart';
import '../features/login_register/presentation/register_cubit/register_cubit.dart';

final sl = GetIt.instance;

Future<void> init() async {
  log('init');
  ///shared preferance
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton<SharedPreferences>(() => sharedPreferences);
  sl.registerLazySingleton<http.Client>(() => http.Client());
  //Remote Data Source
  sl.registerLazySingleton<IFeedRemoteService>(
      () => FeedRemoteApiServiceImpl(sl()));

  ///Network checkker
  sl.registerLazySingleton<Connectivity>(() => Connectivity());
  sl.registerLazySingleton<INetworkStatus>(() => NetworkStatusImpl(sl()));
  
   log('NetworkStatusImpl');
  ///Repository
  sl.registerLazySingleton<IFeedRepository>(
      () => FeedRepositoryImpl(sl(), sl()));
  sl.registerLazySingleton<IAuthRepository>(() => AuthUserRepository(sl()));

  ///Use cases
  sl.registerLazySingleton(() => MovieTrendingUsecase(sl()));

  ///Cubit
  sl.registerLazySingleton<LoginCubit>(() => LoginCubit(sl()));
  sl.registerLazySingleton<RegisterCubit>(() => RegisterCubit(sl()));
  sl.registerLazySingleton<AuthCubit>(() => AuthCubit(sl()));
  sl.registerLazySingleton<MovieTrendingCubit>(() => MovieTrendingCubit(sl()));

  
}
