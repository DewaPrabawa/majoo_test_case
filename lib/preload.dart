import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'di_container/di.dart';
import 'features/feed/presentation/cubit/movie_cubit.dart';
import 'features/feed/presentation/view/home_view.dart';
import 'features/login_register/presentation/auth_cubit/auth_cubit.dart';
import 'features/login_register/presentation/view/login_view.dart';

class Preload extends StatefulWidget {
  const Preload({Key? key}) : super(key: key);

  @override
  State<Preload> createState() => _PreloadState();
}

class _PreloadState extends State<Preload> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
       final authenticationStatus = context.read<AuthCubit>().state.status;
       log("1 ${authenticationStatus}");
    switch (authenticationStatus) {
      case AppStatus.authenticated:
        final homeView = BlocProvider<MovieTrendingCubit>(
          create: (context) => sl<MovieTrendingCubit>()..getFeeds(),
          child: HomeView(),
        );
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => homeView));
          log("2");
        break;
      case AppStatus.unauthenticated:
        const loginView = LoginView();
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => loginView));
          log("3");
        break;
    }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text("Loading...", style: TextStyle(fontSize: 20),),
      ),
    );
  }
}
