class UserAuth {
  final String email;
  final String password;

  const UserAuth(this.email, this.password);

  factory UserAuth.fromMap(Map<String, dynamic> map) {
    return UserAuth(map["email"], map["password"]);
  }

  Map<String, dynamic> toJson(){
    return {"email": email, "password": password};
  }

}
