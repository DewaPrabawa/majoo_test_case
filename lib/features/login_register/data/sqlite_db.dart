import 'package:flutter/foundation.dart';

import 'package:majoo_test_case/shared/exceptions/exceptions.dart';
import 'package:majoo_test_case/shared/final_result.dart';
import 'package:sqflite/sqflite.dart' as sql;

import '../../../shared/login_status.dart';
import 'model/user.dart';

class SqliteDB {
  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE $TABLE_USER_CREDENTIAL(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        email TEXT,
        password TEXT,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }

  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'majoo_test.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  static Future<bool> registerUser(UserAuth userAuth) async {
    if (await _checkEmail(userAuth.email)) {
      return false;
    } else {
      final db = await SqliteDB.db();
      final data = {'email': userAuth.email, 'password': userAuth.password};

      await db.insert(TABLE_USER_CREDENTIAL, data,
          conflictAlgorithm: sql.ConflictAlgorithm.replace);

      return true;
    }
  }

  static Future<Map<String, dynamic>?> getCurrentUser(String email) async {
    final db = await SqliteDB.db();
    List<Map<String, dynamic>> users =
        await db.query(TABLE_USER_CREDENTIAL, orderBy: "id");
    if (users.isEmpty) {
      return null;
    } else {
      return users.singleWhere((element) => element["email"] == email);
    }
  }

  static Future<LoginResponse> login(UserAuth userAuth) async {
    if (await _checkEmail(userAuth.email) &&
        await _checkPassword(userAuth.password)) {
      final loginResponse =
          LoginResponse(loginStatus: LoginStatus.success, userAuth: userAuth);
      return loginResponse;
    }
    if (await _checkEmail(userAuth.email) &&
        !await _checkPassword(userAuth.password)) {
      return LoginResponse(loginStatus: LoginStatus.invalid);
    }
    if (!await _checkEmail(userAuth.email)) {
      return LoginResponse(loginStatus: LoginStatus.unregister);
    }
    return LoginResponse(loginStatus: LoginStatus.nostate);
  }

  static Future<bool> _checkEmail(String email) async {
    final db = await SqliteDB.db();
    List<Map<String, dynamic>> users = await db.query(TABLE_USER_CREDENTIAL,
        where: "email = ?", whereArgs: [email], limit: 1);
    if (users.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

  static Future<bool> _checkPassword(String password) async {
    final db = await SqliteDB.db();
    List<Map<String, dynamic>> users = await db.query(TABLE_USER_CREDENTIAL,
        where: "password = ?", whereArgs: [password], limit: 1);
    if (users.isEmpty) {
      return false;
    } else {
      return true;
    }
  }
}
