import 'dart:convert';
import 'dart:developer';

import 'package:majoo_test_case/shared/final_result.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../shared/login_status.dart';
import '../data/model/user.dart';
import '../data/sqlite_db.dart';

const CACHED_CREDENTIAL = 'CACHED_USER_CREDENTIAL';

abstract class IAuthRepository {
  Future<LoginStatus> login(UserAuth user);
  Future<bool> registerUser(UserAuth user);
  bool hasToken();
  Future<bool> logout();
}

class AuthUserRepository implements IAuthRepository {
  final SharedPreferences sharedPreferences;

  const AuthUserRepository(this.sharedPreferences);

  @override
  Future<LoginStatus> login(UserAuth user) async {
    final loginResponse = await SqliteDB.login(user);
    if (loginResponse.loginStatus == LoginStatus.success) {
      await sharedPreferences.setString(jsonEncode(loginResponse.userAuth?.toJson()), CACHED_CREDENTIAL);
      await sharedPreferences.setBool(CACHED_CREDENTIAL, true);
      return loginResponse.loginStatus;
    } else {
      return loginResponse.loginStatus;
    }
  }

  @override
  Future<bool> registerUser(UserAuth user) async {
    return await SqliteDB.registerUser(user);
  }
  
  @override
  Future<bool> logout() async {
    await sharedPreferences.remove(CACHED_CREDENTIAL);
    return true;
  }

  @override
  bool hasToken()  {
   return sharedPreferences.getBool(CACHED_CREDENTIAL) ?? false;
  }
}
