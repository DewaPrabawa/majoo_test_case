import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:majoo_test_case/shared/login_status.dart';

import '../../data/model/user.dart';
import '../../domain/auth_use_case.dart';
import '../form_model/email.dart';
import '../form_model/password.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit(this._authRepository)
      : super(const LoginState());

  final IAuthRepository _authRepository;

  void emailChanged(String value) {
    if (value.isEmpty) {
      const email = Email.pure();
      emit(
        state.copyWith(
          email: email,
          status: FormzStatus.pure,
        ),
      );
      return;
    }
    final email = Email.dirty(value);
    emit(
      state.copyWith(
        email: email,
        status: Formz.validate([email, state.password]),
      ),
    );
  }

  void passwordChanged(String value) {
    if (value.isEmpty) {
      const password = Password.pure();
      emit(
        state.copyWith(
          password: password,
          status: FormzStatus.pure,
        ),
      );
      return;
    }

    final password = Password.dirty(value);
    emit(
      state.copyWith(
        password: password,
        status: Formz.validate([state.email, password]),
      ),
    );
  }

  Future<void> logInWithCredentials() async {
    if (!state.status.isValidated) return;
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    final LoginStatus loginStatus = await _authRepository
        .login(UserAuth(state.email.value, state.password.value));
    switch (loginStatus) {
      case LoginStatus.success:
        emit(state.copyWith(status: FormzStatus.submissionSuccess));
        clearForm();
        break;
      case LoginStatus.invalid:
        emit(state.copyWith(
            errorMessage: "Your enter a worng password.",
            status: FormzStatus.submissionFailure));
        break;
      case LoginStatus.unregister:
      case LoginStatus.nostate:
      emit(state.copyWith(
          errorMessage:
              "You are not register yet, please create an account first.",
          status: FormzStatus.submissionFailure));
        break;
    }
  }

  void clearForm() async {
    await Future.delayed(const Duration(seconds: 2));
    emit(state.copyWith(
      email: const Email.pure(),
      password: const Password.pure(),
      status: FormzStatus.pure,
    ));
  }
}
