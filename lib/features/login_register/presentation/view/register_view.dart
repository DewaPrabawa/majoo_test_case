import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

import '../register_cubit/register_cubit.dart';

class RegisterView extends StatelessWidget {
  const RegisterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: const BackButton(color: Colors.black),
      ),
      body: BlocListener<RegisterCubit, RegisterState>(
        listener: (context, state) {
          if (state.status.isSubmissionFailure) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  content: Text(
                    state.errorMessage ?? 'Authentication Failure',
                    style: const TextStyle(fontWeight: FontWeight.w800),
                  ),
                ),
              );
          } else if (state.status.isSubmissionSuccess) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(
                const SnackBar(
                  content: Text(
                    "You are officially registered!",
                    style: TextStyle(fontWeight: FontWeight.w800),
                  ),
                ),
              );
              Future.delayed(const Duration(seconds: 3));
              Navigator.popUntil(context, ModalRoute.withName("/login"));
          }
        },
        child: Align(
          alignment: const Alignment(0, -1 / 1.3),
          child: SingleChildScrollView(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset("assets/logo.png"),
              const SizedBox(height: 20),
              _EmailInput(),
              const SizedBox(height: 20),
              _PasswordInput(),
              const SizedBox(height: 25),
              _RegisterButton(),
              const SizedBox(height: 25),
              _BackToLoginButton()
            ],
          )),
        ),
      ),
    );
  }
}

class _EmailInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterCubit, RegisterState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (context, state) {
        return Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          padding: const EdgeInsets.fromLTRB(10, 15, 10, 15),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  blurRadius: 1.0,
                  spreadRadius: 1,
                  offset: const Offset(0, 1),
                  color: Colors.grey.shade300),
            ],
          ),
          child: TextFormField(
            key: const Key('loginForm_emailInput_textField'),
            onChanged: (email) =>
                context.read<RegisterCubit>().emailChanged(email),
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(horizontal: 20),
              isDense: true,
              floatingLabelBehavior: FloatingLabelBehavior.never,
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              focusedErrorBorder: InputBorder.none,
              hintText: 'Enter email',
              hintStyle: const TextStyle(
                  fontWeight: FontWeight.w700, color: Colors.grey),
              errorText: state.email.invalid ? 'invalid email' : null,
            ),
          ),
        );
      },
    );
  }
}

class _PasswordInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterCubit, RegisterState>(
      buildWhen: (previous, current) => previous.password != current.password,
      builder: (context, state) {
        return Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          padding: const EdgeInsets.fromLTRB(10, 15, 10, 15),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                  blurRadius: 1.0,
                  spreadRadius: 1,
                  offset: const Offset(0, 1),
                  color: Colors.grey.shade300),
            ],
          ),
          child: TextFormField(
            key: const Key('registerForm_passwordInput_textField'),
            onChanged: (password) =>
                context.read<RegisterCubit>().passwordChanged(password),
            obscureText: true,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 20,
              ),
              isDense: true,
              floatingLabelBehavior: FloatingLabelBehavior.never,
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              focusedErrorBorder: InputBorder.none,
              hintText: 'Password',
              hintStyle: const TextStyle(
                  fontWeight: FontWeight.w700, color: Colors.grey),
              errorText: state.password.invalid ? 'invalid password' : null,
            ),
          ),
        );
      },
    );
  }
}

class _RegisterButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterCubit, RegisterState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return state.status.isSubmissionInProgress
            ? const CircularProgressIndicator()
            : ElevatedButton(
                key: const Key('registerForm_continue_raisedButton'),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  primary: Color.fromARGB(255, 124, 213, 171),
                ),
                onPressed: state.status.isValidated
                    ? () => context
                        .read<RegisterCubit>()
                        .registerWithCredentials()
                    : null,
                child: const Text(
                  'Sign Up',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
              );
      },
    );
  }
}

class _BackToLoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: const Text(
          "I already have an account register here.",
          style: TextStyle(color: Colors.grey),
        ));
  }
}
