import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:majoo_test_case/features/login_register/presentation/auth_cubit/auth_cubit.dart';
import 'package:majoo_test_case/features/login_register/presentation/login_cubit/login_cubit.dart';
import 'package:majoo_test_case/features/login_register/presentation/view/register_view.dart';
import '../../../../di_container/di.dart';
import '../../../feed/presentation/cubit/movie_cubit.dart';
import '../../../feed/presentation/view/home_view.dart';


class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<LoginCubit, LoginState>(
        listener: (context, state) {
          if(state.status.isSubmissionFailure){
             ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Text(state.errorMessage ?? 'Authentication Failure', style: const TextStyle(fontWeight: FontWeight.w800),),
              ),
            );
          }else if(state.status.isSubmissionSuccess){
             ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              const SnackBar(
                content: Text("Login succesfully!", style: TextStyle(fontWeight: FontWeight.w800),),
              ),
            );
             Future.delayed(const Duration(seconds: 2),(){
               Navigator.pushNamed(context, "/login");
            });
          }
        },
        child: Align(
            alignment: const Alignment(0, -1 / 4),
          child: SingleChildScrollView(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset("assets/logo.png"),
               const SizedBox(height: 20),
              _EmailInput(),
              const SizedBox(height: 20),
              _PasswordInput(),
              const SizedBox(height: 25),
              _LoginButton(),
              const SizedBox(height: 25),
              _RegisterButton(),
            ],
          )),
        ),
      ),
    );
  }
}



class _EmailInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (context, state) {
        return Container(
           margin: const EdgeInsets.symmetric(horizontal: 20),
               padding: const EdgeInsets.fromLTRB(10, 15, 10, 15),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
                 boxShadow: [
              BoxShadow(
                blurRadius: 1.0,
                spreadRadius: 1,
                offset: const Offset(0, 1),
                color: Colors.grey.shade300
              ),
            ],
          ),
          child: TextFormField(
            key: const Key('loginForm_emailInput_textField'),
            onChanged: (email) => context.read<LoginCubit>().emailChanged(email),
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(horizontal: 20),
              isDense: true,
              floatingLabelBehavior: FloatingLabelBehavior.never,
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              focusedErrorBorder: InputBorder.none,
              hintText: 'Enter email',
              hintStyle: const TextStyle(fontWeight: FontWeight.w700, color: Colors.grey),
              errorText: state.email.invalid ? 'invalid email' : null,
            ),
          ),
        );
      },
    );
  }
}

class _PasswordInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      buildWhen: (previous, current) => previous.password != current.password,
      builder: (context, state) {
        return Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          padding: const EdgeInsets.fromLTRB(10, 15, 10, 15),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                blurRadius: 1.0,
                spreadRadius: 1,
                offset: const Offset(0, 1),
                color: Colors.grey.shade300
              ),
            ],
          ),
          child: TextFormField(
            key: const Key('loginForm_passwordInput_textField'),
            onChanged: (password) =>
                context.read<LoginCubit>().passwordChanged(password),
            obscureText: true,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(horizontal: 20,),
              isDense: true,
               floatingLabelBehavior: FloatingLabelBehavior.never,
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              focusedErrorBorder: InputBorder.none,
              hintText: 'Password',
              hintStyle: const TextStyle(fontWeight: FontWeight.w700, color: Colors.grey),
              errorText: state.password.invalid ? 'invalid password' : null,
            ),
          ),
        );
      },
    );
  }
}


class _LoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return state.status.isSubmissionInProgress
            ? const CircularProgressIndicator()
            : ElevatedButton(
                key: const Key('loginForm_continue_raisedButton'),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  primary: const Color(0xFFFD6B68),
                ),
                onPressed: state.status.isValidated
                    ? () => context.read<LoginCubit>().logInWithCredentials()
                    : null,
                child: const Text('Sign In', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),),
              );
      },
    );
  }
}


class _RegisterButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.pushNamed(context, "/register");
      },
      child: const SizedBox(
        height: 50,
        child: Text("Do not have account register here.", style: TextStyle(color: Colors.grey),)));
  }
}
