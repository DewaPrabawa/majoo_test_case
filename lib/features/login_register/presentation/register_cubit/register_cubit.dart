import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:majoo_test_case/features/login_register/data/model/user.dart';
import 'package:majoo_test_case/features/login_register/domain/auth_use_case.dart';
import 'package:majoo_test_case/features/login_register/presentation/form_model/email.dart';
import 'package:majoo_test_case/features/login_register/presentation/form_model/password.dart';

part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit(this._authRepository) : super(const RegisterState());

  final IAuthRepository _authRepository;


  void emailChanged(String value) {
    if (value.isEmpty) {
      const email = Email.pure();
      emit(
        state.copyWith(
          email: email,
          status: FormzStatus.pure,
        ),
      );
      return;
    }
    final email = Email.dirty(value);
    emit(
      state.copyWith(
        email: email,
        status: Formz.validate([email, state.password]),
      ),
    );
  }

  void passwordChanged(String value) {
    if (value.isEmpty) {
      const password = Password.pure();
      emit(
        state.copyWith(
          password: password,
          status: FormzStatus.pure,
        ),
      );
      return;
    }

    final password = Password.dirty(value);
    emit(
      state.copyWith(
        password: password,
        status: Formz.validate([state.email, password]),
      ),
    );
  }

   Future<void> registerWithCredentials() async {
    if (!state.status.isValidated) return;
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    final bool succes = await _authRepository
        .registerUser((UserAuth(state.email.value, state.password.value)));
    if (succes) {
      emit(state.copyWith(status: FormzStatus.submissionSuccess));
    } else {
      emit(state.copyWith(
          errorMessage: "The Account as already register.",
          status: FormzStatus.submissionFailure));
    }
  }
}
