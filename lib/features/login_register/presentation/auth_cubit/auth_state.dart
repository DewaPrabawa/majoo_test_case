part of 'auth_cubit.dart';

enum AppStatus {
  authenticated,
  unauthenticated,
}

class AuthState extends Equatable {
  const AuthState._({
    required this.status,
  });

  const AuthState.authenticated()
      : this._(status: AppStatus.authenticated);

  const AuthState.unauthenticated() : this._(status: AppStatus.unauthenticated);

  final AppStatus status;

  @override
  List<Object?> get props => [status, ];
}