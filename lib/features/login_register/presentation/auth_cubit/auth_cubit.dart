import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majoo_test_case/features/login_register/data/model/user.dart';
import 'package:majoo_test_case/features/login_register/domain/auth_use_case.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit(this._authRepository) : super(const AuthState.unauthenticated());

  final IAuthRepository _authRepository;

  void isLoggin() {
    final bool isLoggedIn = _authRepository.hasToken();
    if (isLoggedIn) {
      emit(const AuthState.authenticated());
    }else{
      emit(const AuthState.unauthenticated());
    }
  }

  Future<void> logout() async {
    if(await _authRepository.logout()){
      emit(const AuthState.unauthenticated());
    }
  }
}
