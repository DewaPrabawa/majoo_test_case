import 'package:majoo_test_case/features/feed/data/repository/feed_repository.dart';
import 'package:majoo_test_case/shared/final_result.dart';
import 'package:majoo_test_case/shared/usecase.dart';

import '../model/data.dart';

class MovieTrendingUsecase implements UseCase<FinalResult<Data>, NoParams>{

  final IFeedRepository _feedRepository;

  MovieTrendingUsecase(this._feedRepository);

  @override
  Future<FinalResult<Data>> call({NoParams? params}) async {
    return await _feedRepository.getFeeds();
  }
  
}