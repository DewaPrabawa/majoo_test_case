import 'dart:convert';

class Data {
    const Data({
        this.page = 0,
        this.results = const [],
        this.totalPages = 0,
        this.totalResults = 0,
    });

    final int page;
    final List<Result> results;
    final int totalPages;
    final int totalResults;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        page: json["page"],
        results: List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        totalPages: json["total_pages"],
        totalResults: json["total_results"],
    );

   static const empty = Data(page: 0, results: [], totalPages: 0, totalResults: 0); 
}

class Result {
    Result({
        this.overview,
        this.releaseDate,
        this.title,
        this.adult,
        this.backdropPath,
        this.genreIds,
        this.originalLanguage,
        this.originalTitle,
        this.posterPath,
        this.voteCount,
        this.video,
        this.id,
        this.voteAverage,
        this.popularity,
        this.mediaType,
        this.firstAirDate,
        this.name,
        this.originalName,
        this.originCountry,
    });

    String? overview;
    DateTime? releaseDate;
    String? title;
    bool? adult;
    String? backdropPath;
    List<int>? genreIds;
    OriginalLanguage? originalLanguage;
    String? originalTitle;
    String? posterPath;
    int? voteCount;
    bool? video;
    int? id;
    double? voteAverage;
    double? popularity;
    MediaType? mediaType;
    DateTime? firstAirDate;
    String? name;
    String? originalName;
    List<String>? originCountry;

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        overview: json["overview"],
        releaseDate: json["release_date"] == null ? null : DateTime.parse(json["release_date"]),
        title: json["title"] ?? json["original_title"] ?? "Unknown",
        adult: json["adult"] ?? false,
        backdropPath: "https://image.tmdb.org/t/p/w500"+json["backdrop_path"],
        genreIds: List<int>.from(json["genre_ids"].map((x) => x)),
        originalLanguage: originalLanguageValues.map?[json["original_language"]],
        originalTitle: json["original_title"] ?? "Unknown",
        posterPath:"https://image.tmdb.org/t/p/w500"+json["poster_path"],
        voteCount: json["vote_count"],
        video: json["video"] ?? false,
        id: json["id"],
        voteAverage: json["vote_average"].toDouble(),
        popularity: json["popularity"].toDouble(),
        mediaType: mediaTypeValues.map?[json["media_type"]],
        firstAirDate: json["first_air_date"] == null ? null : DateTime.parse(json["first_air_date"]),
        name: json["name"] ?? "unknown",
        originalName: json["original_name"] ?? "unknown",
        originCountry: json["origin_country"] == null ? null : List<String>.from(json["origin_country"].map((x) => x)),
    );

}

enum MediaType { MOVIE, TV }

final mediaTypeValues = EnumValues({
    "movie": MediaType.MOVIE,
    "tv": MediaType.TV
});

enum OriginalLanguage { EN, DA, PL }

final originalLanguageValues = EnumValues({
    "da": OriginalLanguage.DA,
    "en": OriginalLanguage.EN,
    "pl": OriginalLanguage.PL
});

class EnumValues<T> {
    Map<String, T>? map;
    Map<T, String>? reverseMap;

    EnumValues(this.map);

    Map<T, String>? get reverse {
        if (reverseMap != null) {
          return reverseMap;  
        }else{
          return reverseMap = map?.map((k, v) => MapEntry(v, k));
        }
     
    }
}
