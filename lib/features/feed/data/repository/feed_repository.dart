import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart';
import 'package:majoo_test_case/features/feed/data/remote_service/feed_remote_service.dart';
import 'package:majoo_test_case/features/feed/model/data.dart';
import 'package:majoo_test_case/shared/final_result.dart';
import 'package:majoo_test_case/shared/network_status.dart';

import '../../../../shared/exceptions/exceptions.dart';

abstract class IFeedRepository {
  Future<FinalResult<Data>> getFeeds();
}

class FeedRepositoryImpl implements IFeedRepository {
  final IFeedRemoteService _feedRemoteService;
  final INetworkStatus _networkStatus;

  FeedRepositoryImpl(this._feedRemoteService, this._networkStatus);

  @override
  Future<FinalResult<Data>> getFeeds() async {
    log("getFeeds");
    if (await _networkStatus.isConnected) {
      try {
        final response = await _feedRemoteService.getFeeds();
        final data = Data.fromJson(jsonDecode(response.body));
        // log("getFeeds = ${data.page}");
        return FinalResult(isSuccess: true, data: data);
      } on RemoteServerException catch (_) {
        const message = "Something goes wrong on the server!";
        return const FinalResult(isSuccess: false, message: message);
      }
    }else{
      const message = "There is no connection!";
      return const FinalResult(isSuccess: false, message: message);
    }
  }
}
