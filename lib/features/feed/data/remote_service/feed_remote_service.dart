import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import '../../../../shared/exceptions/exceptions.dart';

const STATUS_OK = 200;

abstract class IFeedRemoteService {
  Future<http.Response> getFeeds();
}

class FeedRemoteApiServiceImpl implements IFeedRemoteService {

  final http.Client client;

  FeedRemoteApiServiceImpl(this.client);

  @override
  Future<http.Response> getFeeds() async {
    final Uri url =
        Uri.parse("https://api.themoviedb.org/3/trending/all/day?api_key=0bc9e6490f0a9aa230bd01e268411e10");
    try {
      http.Response response = await client.get(url, headers: {
        'Content-Type': 'application/json',
      });
      if (response.statusCode != STATUS_OK) {
        throw HttpRequestException();
      } else {
        // log(response.body.toString());
        return response;
      }
    } catch (_) {
        throw RemoteServerException();
    }
  }
  
  }