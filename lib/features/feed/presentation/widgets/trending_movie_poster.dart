import 'package:flutter/material.dart';

import '../cubit/movie_cubit.dart';
import '../view/detail_movie.dart';

class BuildTrendingMoviewPoster extends StatelessWidget {
  const BuildTrendingMoviewPoster(
      {Key? key, required this.screenWidth, required this.state})
      : super(key: key);

  final double? screenWidth;
  final MovieTrendingState state;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Text(
            "Popular Movie",
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
          ),
        ),
        SizedBox(
          height: 200,
          child: ListView.builder(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
              ),
              itemCount: state.count,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: (){
                    Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            DetailMovie(details: state.movies[index])));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Image.network(
                          state.movies[index].posterPath!,
                          height: 80,
                          width: (screenWidth! - 60),
                          fit: BoxFit.cover,
                        )),
                  ),
                );
              }),
        )
      ],
    );
  }
}
