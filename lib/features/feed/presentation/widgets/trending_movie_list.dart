import 'package:flutter/material.dart';
import 'package:majoo_test_case/features/feed/presentation/cubit/movie_cubit.dart';
import 'package:majoo_test_case/features/feed/presentation/view/detail_movie.dart';

class BuildListTrendingMovie extends StatelessWidget {
  const BuildListTrendingMovie({Key? key, required this.state})
      : super(key: key);

  final MovieTrendingState state;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          itemCount: state.count,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            DetailMovie(details: state.movies[index])));
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 2,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image.network(
                            state.movies[index].posterPath!,
                            height: 150,
                            fit: BoxFit.fill,
                          )),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            state.movies[index].title!,
                            style: const TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                                fontSize: 18),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            state.movies[index].overview!,
                            style: const TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Colors.grey,
                                fontSize: 14),
                            maxLines: 5,
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        margin: const EdgeInsets.only(left: 10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(width: 1, color: Colors.grey)),
                        child: const Center(child: Text("Detail")),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}

