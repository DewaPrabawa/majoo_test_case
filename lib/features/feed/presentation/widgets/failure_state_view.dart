import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/movie_cubit.dart';

class BuildFailureState extends StatelessWidget {
  const BuildFailureState({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final currentStatus = context.read<MovieTrendingCubit>().state.status;
    final errorMessage = context.read<MovieTrendingCubit>().state.errorMessage;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
       Image.asset("assets/no_connection.png"),
       const SizedBox(height: 20),
       Text(errorMessage, style: const TextStyle(
                fontWeight: FontWeight.w600, color: Colors.black, fontSize: 28),),
       const SizedBox(height: 20),
       currentStatus == StateStatus.loading ? const CircularProgressIndicator()
            : Center(
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    primary: const Color(0xFFFD6B68),
                  ),
                  onPressed: currentStatus == StateStatus.failure
                      ? () => context.read<MovieTrendingCubit>().getFeeds()
                      : null,
                  child: const Text('Refresh', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),),
                ),
            )
      ],
    );
  }
}
