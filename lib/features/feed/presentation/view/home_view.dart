import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo_test_case/features/feed/presentation/view/detail_movie.dart';
import 'package:majoo_test_case/features/feed/presentation/cubit/movie_cubit.dart';
import 'package:majoo_test_case/features/login_register/data/model/user.dart';

import 'package:majoo_test_case/preload.dart';
import '../../../login_register/presentation/auth_cubit/auth_cubit.dart';
import '../../../login_register/presentation/login_cubit/login_cubit.dart';
import '../widgets/failure_state_view.dart';
import '../widgets/trending_movie_list.dart';
import '../widgets/trending_movie_poster.dart';

class HomeView extends StatelessWidget {
  HomeView({Key? key}) : super(key: key);

  late double? screenWidth;
  late String? email;

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    email = context.read<LoginCubit>().state.email.value;
    return Scaffold(
      body: SafeArea(child: _buildBody()),
    );
  }

  Future<void> _showMyDialog(BuildContext context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('hey!'),
        content: SingleChildScrollView(
          child: ListBody(
            children: const <Widget>[
              Text('Do you really want to logout ?'),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('Logout'),
            onPressed: () async {
              await context.read<AuthCubit>().logout();
              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => const Preload()),(route) => false);
            },
          ),
            TextButton(
            child: const Text('Later'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    },
  );
}


  Widget _buildAppBar(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            "Movie",
            style: TextStyle(
                fontWeight: FontWeight.w600, color: Colors.black, fontSize: 28),
          ),
          InkWell(
            onTap: (){
              _showMyDialog(context);
            },
            child: const Text(
              "Sign out",
              style: TextStyle(
                  fontWeight: FontWeight.w800, color: Colors.black, fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHomeViewLayout(BuildContext context, MovieTrendingState state){
  return Stack(children: [_buildTrending(state), _buildAppBar(context)]);
}


  Widget _buildBody() {
    return BlocBuilder<MovieTrendingCubit, MovieTrendingState>(
      builder: (context, state) {
        switch (state.status) {
          case StateStatus.initial:
          case StateStatus.loading:
            return _buildLoading();
          case StateStatus.success:
            return _buildHomeViewLayout(context, state);
          case StateStatus.failure:
          case StateStatus.empty:
            return const BuildFailureState();
        }
      },
    );
  }

  Widget _buildLoading() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _buildTrending(MovieTrendingState state) {
    return Padding(
      padding: const EdgeInsets.only(top: 50),
      child: Column(
        children: [
          BuildTrendingMoviewPoster(
            screenWidth: screenWidth,
            state: state,
          ),
          BuildListTrendingMovie(
            state: state,
          )
        ],
      ),
    );
  }
}

