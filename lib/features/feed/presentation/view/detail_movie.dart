import 'package:flutter/material.dart';
import 'package:majoo_test_case/features/feed/model/data.dart';
import 'package:majoo_test_case/features/feed/presentation/cubit/movie_cubit.dart';

class DetailMovie extends StatelessWidget {
  const DetailMovie({Key? key, required this.details}) : super(key: key);

  final Result details;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: const BackButton(color: Colors.black),
      ),
      body: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
              borderRadius: const BorderRadius.only(
                  bottomRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20)),
              child: Image.network(
                details.posterPath!,
                fit: BoxFit.fill,
              )),
          const SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start
              ,children: [
              Text(
              details.title!,
              style: const TextStyle(
                  fontSize: 20, color: Colors.black, fontWeight: FontWeight.w900),
            ),
            const SizedBox(
              height: 5,
            ),
            const Divider(),
            const SizedBox(
              height: 10,
            ),
            Text(
                "Release - ${details.releaseDate!.day}/${details.releaseDate!.month}/${details.releaseDate!.year}",
                style: const TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Colors.black,
                    fontSize: 14)),
            const SizedBox(
              height: 8,
            ),
              Text(
                "Popularity(${details.popularity!.toInt()})",
                style: const TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Colors.black,
                    fontSize: 14)),
              const SizedBox(
               height: 8,
            ),
              Text(
                "Rating(${details.voteAverage})",
                style: const TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Colors.black,
                    fontSize: 14)),        
            const SizedBox(
               height: 8,
            ),
            const Divider(),
            Text(details.overview!,
                style: const TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Colors.grey,
                    fontSize: 14)),
            ],),
          ),
          const SizedBox(height: 30,),        
        ],
      )),
    );
  }
}
