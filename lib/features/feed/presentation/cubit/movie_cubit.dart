import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majoo_test_case/features/feed/domain/feed_usecase.dart';
import 'package:majoo_test_case/shared/final_result.dart';

import '../../model/data.dart';

part 'movie_state.dart';

class MovieTrendingCubit extends Cubit<MovieTrendingState> {
  MovieTrendingCubit(this._movieTrendingUsecase):super(const MovieTrendingState());

  final MovieTrendingUsecase _movieTrendingUsecase;

  void getFeeds() async {
    emit(state.copyWith(status: StateStatus.loading));
    final result = await _movieTrendingUsecase.call();
    if (result.isSuccess) {
      if(result.data!.results.isEmpty){
        emit(state.copyWith(data: result.data, status: StateStatus.empty));
      }else{
        emit(state.copyWith(data: result.data, status: StateStatus.success));
      }
    } else {
      emit(state.copyWith(status: StateStatus.failure, errorMessage: result.message));
    }
  }
}
