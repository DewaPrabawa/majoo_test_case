part of 'movie_cubit.dart';

enum StateStatus { initial, loading, success, failure, empty }

class MovieTrendingState extends Equatable {
  const MovieTrendingState(
      {this.status = StateStatus.initial,
      this.data =  Data.empty,
      this.errorMessage = ""});

  final StateStatus status;
  final Data data;
  final String errorMessage;

  int get count => data.results.length;

  List<Result> get movies => data.results;

  MovieTrendingState copyWith({StateStatus? status, Data? data, String? errorMessage}) {
    log("FeedState ${data?.results.length}");
    return MovieTrendingState(
        status: status ?? this.status,
        data: data ?? this.data,
        errorMessage: errorMessage ?? this.errorMessage);
  }

  @override
  List<Object> get props => [status, data, errorMessage];
}
