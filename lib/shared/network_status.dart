import 'package:connectivity_plus/connectivity_plus.dart';

abstract class INetworkStatus {
  Future<bool> get isConnected;
}

class NetworkStatusImpl implements INetworkStatus {
  final Connectivity connectionChecker;

  NetworkStatusImpl(this.connectionChecker);

  Future<bool> _isConnecting() async {
    final connectionResult = await connectionChecker.checkConnectivity();
    switch(connectionResult){
      case ConnectivityResult.bluetooth:
      case ConnectivityResult.wifi:
      case ConnectivityResult.ethernet:
      case ConnectivityResult.mobile:
       return true;
      case ConnectivityResult.none:
       return false;
    }
  }

  @override
  Future<bool> get isConnected => _isConnecting();
}