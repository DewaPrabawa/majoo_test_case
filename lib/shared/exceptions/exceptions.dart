
//Exception that is thrown when server occurs failure
class RemoteServerException implements Exception {}

//Exception that is thrown when http request call response is not 200
class HttpRequestException implements Exception {}

class UserCreationIsFailed implements Exception {}

class UserLogginIsFailed implements Exception {}
