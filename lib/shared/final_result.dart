import 'package:majoo_test_case/features/login_register/data/model/user.dart';
import 'package:majoo_test_case/shared/login_status.dart';

class FinalResult<T> {
  final bool isSuccess;
  final T? data;
  final String? message;

  const FinalResult({required this.isSuccess, this.message, this.data});
}

class LoginResponse {
  final LoginStatus loginStatus;
  final UserAuth? userAuth;

  LoginResponse({required this.loginStatus, this.userAuth});
}